## Cidenet registro de empleados API REST

Solución de prueba tecnica CIDENET para puesto desarrollador BackEnd Java por Alejandro Hincapie Pulgarin

## Try it
La documentación de los API endpoints de la aplicación se genero con Swagger, para acceder: http://localhost:8080/swagger-ui/index.html
(puerto 8080 por defecto de SpringBoot)

## Prerrequisitos
-JDK 17

-MySQL

-Maven 3.8.6

-IDE Eclipse 2022-12 (O el IDE de su preferencia)

## Herramientas usadas
Para la creación de este proyecto se uso Java 17, SpringBoot 3.0.0 y Maven mediante la pagina oficial https://start.spring.io/ 

Las dependencias instaladas fueron: Spring Web, Spring Data JPA, MySQL Driver, Validation, Spring Boot DevTools

Se importo el poryecto a eclipse como un maven proyect y se le agrego la dependencia https://springdoc.org/v2/ para la generación de documentación

## Instalación
En la aplicación MySQL Workbench crear conexión con usuario: cidenet, pasword: cidenet 
en el localhost:3306

usar el Script https://gitlab.com/AlejandroHincapie/api_cidenet_empleados/-/blob/main/createDB.sql para inicializar la data base. 

Importar el proyecto al IDE como Maven project, cuando las dependencias se instalen correctamente ir a la aplicacion principal src/main/java/RegistroempleadosApplication.java y ejecutarla como java aplication. 

Si desea cambiar los puertos de despliegue de la aplicacion o de conexion en el archivo src/main/resources/aplication.properties






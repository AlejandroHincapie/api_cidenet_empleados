package com.cidenetregistroempleados.registroempleados;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.cidenetregistroempleados.registroempleados.entity.Empleado;
import com.cidenetregistroempleados.registroempleados.service.ServicioEmpleado;

@SpringBootTest
class RegistroempleadosApplicationTests {

	@Autowired
	private ServicioEmpleado servicioEmpleado;

	@DisplayName("Test para prueba generacion de emails")
	@Test
	void test1() throws Exception {

		Empleado empleado = new Empleado();

		// "ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUCTURA", "OPERACION",
		// "TALENTO HUMANO", "SERVICIOS VARIOS"
		empleado.setArea("ADMINISTRACION");

		// se debe autogenerar
		empleado.setEmail("PRUEBA@123");

		// "ACTIVO", "INACTIVO" , ACTIVO por defecto
		empleado.setEstado("INACTIVO");

		// pattern = "dd-MM-yyyy", no puede ser en el futuro

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		empleado.setFechaIngreso(LocalDate.parse("08-10-2021", formatter));

		// se debe autogenerar
		empleado.setFechaRegistro(LocalDateTime.parse("2018-05-05T11:50:55"));

		// se debe autogenerar
		// empleado.setId();

		// max 20, no caracteres especiales
		empleado.setNumeroIdentificacion("1234585478");

		// max 50, no obligatorio
		empleado.setOtrosNombres(null);

		// "COL", "USA" , no vacio
		empleado.setPaisEmpleo("USA");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado.setPrimerApellido("EL PEREZ");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado.setPrimerNombre("JUAN");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado.setSegundoApellido("CALVO");

		// "CEDULA DE CIUDADANIA",
		// "CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL"
		empleado.setTipoIdentificacion("PASAPORTE");

		servicioEmpleado.save(empleado);

		Empleado empleado2 = new Empleado();

		// "ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUCTURA", "OPERACION",
		// "TALENTO HUMANO", "SERVICIOS VARIOS"
		empleado2.setArea("ADMINISTRACION");

		// se debe autogenerar
		empleado2.setEmail("PRUEBA@123");

		// "ACTIVO", "INACTIVO" , ACTIVO por defecto
		empleado2.setEstado("INACTIVO");

		// pattern = "dd-MM-yyyy", no puede ser en el futuro

		empleado2.setFechaIngreso(LocalDate.parse("08-10-2020", formatter));

		// se debe autogenerar
		empleado2.setFechaRegistro(LocalDateTime.parse("2018-05-05T11:50:55"));

		// se debe autogenerar
		// empleado.setId();

		// max 20, no caracteres especiales
		empleado2.setNumeroIdentificacion("1234585478");

		// max 50, no obligatorio
		empleado2.setOtrosNombres(null);

		// "COL", "USA" , no vacio
		empleado2.setPaisEmpleo("USA");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado2.setPrimerApellido("EL PEREZ");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado2.setPrimerNombre("JUAN");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado2.setSegundoApellido("CALVO");

		// "CEDULA DE CIUDADANIA",
		// "CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL"
		empleado2.setTipoIdentificacion("PASAPORTE");

		servicioEmpleado.save(empleado2);

		assertNotEquals(empleado.getEmail(), empleado2.getEmail());

	}

	@DisplayName("FALLA POR VALIDACION")
	@Test
	void test2() throws Exception {

		Empleado empleado3 = new Empleado();

		// "ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUCTURA", "OPERACION",
		// "TALENTO HUMANO", "SERVICIOS VARIOS"
		empleado3.setArea("ADMIISTRACION");

		// se debe autogenerar
		empleado3.setEmail("PRUEBA@123");

		// "ACTIVO", "INACTIVO" , ACTIVO por defecto
		empleado3.setEstado("INACTIVO");

		// pattern = "dd-MM-yyyy", no puede ser en el futuro

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		empleado3.setFechaIngreso(LocalDate.parse("08-10-2023", formatter));

		// se debe autogenerar
		empleado3.setFechaRegistro(LocalDateTime.parse("2018-05-05T11:50:55"));

		// se debe autogenerar
		// empleado.setId();

		// max 20, no caracteres especiales
		empleado3.setNumeroIdentificacion(
				"12345ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff85478");

		// max 50, no obligatorio
		empleado3.setOtrosNombres(null);

		// "COL", "USA" , no vacio
		empleado3.setPaisEmpleo("UA");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado3.setPrimerApellido("EL Pddddd dd 34324$$$EREZ:");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado3.setPrimerNombre("JUAN////?");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado3.setSegundoApellido("CALVfs7d5f5s6d4f86se4f86swO");

		// "CEDULA DE CIUDADANIA",
		// "CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL"
		empleado3.setTipoIdentificacion("PASAPORTE");

		servicioEmpleado.save(empleado3);

	}

	@DisplayName("Generacion de campos automaticos")
	@Test
	void test3() throws Exception {

		Empleado empleado4 = new Empleado();

		// "ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUCTURA", "OPERACION",
		// "TALENTO HUMANO", "SERVICIOS VARIOS"
		empleado4.setArea("ADMINISTRACION");

		// se debe autogenerar
		empleado4.setEmail("PRUEBA@123");

		// "ACTIVO", "INACTIVO" , ACTIVO por defecto
		empleado4.setEstado("INACTIVO");

		// pattern = "dd-MM-yyyy", no puede ser en el futuro

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		empleado4.setFechaIngreso(LocalDate.parse("08-10-2021", formatter));

		// se debe autogenerar
		empleado4.setFechaRegistro(LocalDateTime.parse("2018-05-05T11:50:55"));

		// se debe autogenerar
		// empleado.setId();

		// max 20, no caracteres especiales
		empleado4.setNumeroIdentificacion("1234585478");

		// max 50, no obligatorio
		empleado4.setOtrosNombres(null);

		// "COL", "USA" , no vacio
		empleado4.setPaisEmpleo("USA");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado4.setPrimerApellido("EL PEREZ");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado4.setPrimerNombre("JUAN");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado4.setSegundoApellido("CALVO");

		// "CEDULA DE CIUDADANIA",
		// "CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL"
		empleado4.setTipoIdentificacion("PASAPORTE");

		servicioEmpleado.save(empleado4);

		assertEquals("ACTIVO", empleado4.getEstado());
		assertNotEquals("PRUEBA@123", empleado4.getEmail());

	}

	@DisplayName("Test update")
	@Test
	void test4() throws Exception {

		Empleado empleado5 = new Empleado();

		// "ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUCTURA", "OPERACION",
		// "TALENTO HUMANO", "SERVICIOS VARIOS"
		empleado5.setArea("ADMINISTRACION");

		// se debe autogenerar
		empleado5.setEmail("PRUEBA@123");

		// "ACTIVO", "INACTIVO" , ACTIVO por defecto
		empleado5.setEstado("INACTIVO");

		// pattern = "dd-MM-yyyy", no puede ser en el futuro

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		empleado5.setFechaIngreso(LocalDate.parse("08-10-2021", formatter));

		// se debe autogenerar
		empleado5.setFechaRegistro(LocalDateTime.parse("2018-05-05T11:50:55"));

		// se debe autogenerar
		empleado5.setId(1);

		// max 20, no caracteres especiales
		empleado5.setNumeroIdentificacion("1234585478");

		// max 50, no obligatorio
		empleado5.setOtrosNombres(null);

		// "COL", "USA" , no vacio
		empleado5.setPaisEmpleo("USA");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado5.setPrimerApellido("HERNANDEZ");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado5.setPrimerNombre("LUCIO");

		// no caracteres especiales, mayusculas, max 20, no vacio
		empleado5.setSegundoApellido("CALVO");

		// "CEDULA DE CIUDADANIA",
		// "CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL"
		empleado5.setTipoIdentificacion("PASAPORTE");

		servicioEmpleado.saveUpdate(empleado5);

		assertEquals("LUCIO", empleado5.getPrimerNombre());
		assertEquals("HERNANDEZ", empleado5.getPrimerApellido());
		assertEquals(1, empleado5.getId());
		assertNotEquals("juan.elperez@cidenet.com.us", empleado5.getEmail());

	}

	@DisplayName("USER NOT FOUND EXCEPTION Test Delete")
	@Test
	void test5() throws Exception {

		servicioEmpleado.deleteById(1);

		//lanza excepcion 
		servicioEmpleado.findById(1);

	}

}

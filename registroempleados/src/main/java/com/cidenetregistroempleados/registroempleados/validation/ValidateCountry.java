package com.cidenetregistroempleados.registroempleados.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidadorEmpleadoPais.class )
public @interface ValidateCountry {

	public String message() default "Region de empleado invalida : debe ser seleccionada entre COL (Colombia) o USA (Estados Unidos)"; 
	
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}

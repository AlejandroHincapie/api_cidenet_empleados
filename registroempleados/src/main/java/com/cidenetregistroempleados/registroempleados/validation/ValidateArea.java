package com.cidenetregistroempleados.registroempleados.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidadorEmpleadoArea.class )
public @interface ValidateArea {

	public String message() default "Area de empleado invalida : debe ser seleccionada entre: ADMINISTRACIO, FINANCIERA"
			+ "COMPRAS, INFRAESTRUCTURA, OPERACION, TALENTO HUMANO, SERVICIOS VARIOS"; 
	
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}

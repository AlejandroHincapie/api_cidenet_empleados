package com.cidenetregistroempleados.registroempleados.validation;

import java.util.Arrays;
import java.util.List;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ValidadorEmpleadoEstado implements ConstraintValidator<ValidateEstado, String> {

	@Override
	public boolean isValid(String estado, ConstraintValidatorContext context) {

		List<String> estados = Arrays.asList("ACTIVO", "INACTIVO");

		return estados.contains(estado);
	}

}
package com.cidenetregistroempleados.registroempleados.validation;

import java.util.Arrays;
import java.util.List;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ValidadorEmpleadoIdentificacion implements ConstraintValidator<ValidateIdentificacion, String> {

	@Override
	public boolean isValid(String identificacion, ConstraintValidatorContext context) {

		List<String> identificaciones= Arrays.asList("CEDULA DE CIUDADANIA",
				"CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL");

		return identificaciones.contains(identificacion);
	}

}
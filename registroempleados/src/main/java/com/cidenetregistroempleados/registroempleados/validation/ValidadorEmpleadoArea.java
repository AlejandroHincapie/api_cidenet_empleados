package com.cidenetregistroempleados.registroempleados.validation;

import java.util.Arrays;
import java.util.List;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ValidadorEmpleadoArea implements ConstraintValidator<ValidateArea, String> {

	@Override
	public boolean isValid(String area, ConstraintValidatorContext context) {

		List<String> areas = Arrays.asList("ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUCTURA", "OPERACION",
				"TALENTO HUMANO", "SERVICIOS VARIOS");

		return areas.contains(area);
	}

}

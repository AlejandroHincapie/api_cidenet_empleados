package com.cidenetregistroempleados.registroempleados.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidadorEmpleadoIdentificacion.class )
public @interface ValidateIdentificacion {

	public String message() default "Identificacion del empleado debe ser: CEDULA DE CIUDADANIA, CEDULA DE EXTRANJERIA,"
			+ " PASAPORTE, PERMISO ESPECIAL" ; 
	
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}

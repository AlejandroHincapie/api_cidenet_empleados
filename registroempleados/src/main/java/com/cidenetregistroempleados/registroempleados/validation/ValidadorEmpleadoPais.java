package com.cidenetregistroempleados.registroempleados.validation;

import java.util.Arrays;
import java.util.List;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ValidadorEmpleadoPais implements ConstraintValidator<ValidateCountry, String> {

	@Override
	public boolean isValid(String pais, ConstraintValidatorContext context) {

		List<String> paises = Arrays.asList("COL", "USA");

		return paises.contains(pais);
	}

}

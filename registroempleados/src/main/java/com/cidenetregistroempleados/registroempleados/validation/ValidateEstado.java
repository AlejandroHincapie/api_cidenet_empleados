package com.cidenetregistroempleados.registroempleados.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidadorEmpleadoEstado.class )
public @interface ValidateEstado {

	public String message() default "Estado del empleado invalido, debe ser seleccionado entre ACTIVO o INACTIVO"; 
	
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}

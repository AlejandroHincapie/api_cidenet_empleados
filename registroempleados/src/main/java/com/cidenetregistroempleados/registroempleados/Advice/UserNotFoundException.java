package com.cidenetregistroempleados.registroempleados.Advice;

import java.lang.Exception;

public class UserNotFoundException extends Exception {

	public UserNotFoundException(String message) {
		super(message);

	}

}

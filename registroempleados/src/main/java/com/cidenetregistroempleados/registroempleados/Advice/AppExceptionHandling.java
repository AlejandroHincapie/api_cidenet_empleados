package com.cidenetregistroempleados.registroempleados.Advice;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AppExceptionHandling {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> invalidArguments(MethodArgumentNotValidException ex) {
		Map<String, String> errorMap = new HashMap<>();

		ex.getBindingResult().getAllErrors().forEach(error -> {
			errorMap.put(((FieldError) error).getField(), error.getDefaultMessage());
		});

		return errorMap;

	}
	

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PropertyReferenceException.class)
	public Map<String, String> referenciaInvalida(PropertyReferenceException ex) {

		Map<String, String> errorMap = new HashMap<>();
		errorMap.put("errorMessage", ex.getMessage());

		return errorMap;
	}
	
	
	
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(IllegalArgumentException.class)
	public Map<String, String> referenciaInvalida(IllegalArgumentException ex) {

		Map<String, String> errorMap = new HashMap<>();
		errorMap.put("errorMessage", ex.getMessage());

		return errorMap;
	}
	

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(UserNotFoundException.class)
	public Map<String, String> handleExceptions(UserNotFoundException ex) {

		Map<String, String> errorMap = new HashMap<>();
		errorMap.put("errorMessage", ex.getMessage());

		return errorMap;

	}
	
	
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public Map<String, String> entradainvalida(HttpMessageNotReadableException ex) {

		Map<String, String> errorMap = new HashMap<>();
		errorMap.put("Error asegurese de ingresar la fecha de la forma dd-MM-yyyy", ex.getMessage());

		return errorMap;
	}
	
	
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(DateTimeParseException.class)
	public Map<String, String> entradainvaliDadate(DateTimeParseException ex) {

		Map<String, String> errorMap = new HashMap<>();
		errorMap.put("Error asegurese de ingresar la fecha de la forma dd-MM-yyyy", ex.getMessage());

		return errorMap;
	}

}

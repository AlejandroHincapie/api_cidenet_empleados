package com.cidenetregistroempleados.registroempleados.service;

import java.util.List;

import org.springframework.data.domain.Page;


import com.cidenetregistroempleados.registroempleados.entity.Empleado;

public interface ServicioEmpleado {
	
	
	public List<Empleado> findAll(); 
	public Empleado findById(int id) throws Exception; 
	public void save (Empleado empleado) throws Exception;
	public void deleteById(int Id) throws Exception; 
	public Page<Empleado> findSorting (Integer offset, Integer pageSize, String field); 
	public void saveUpdate(Empleado empleado) throws Exception; 
}

package com.cidenetregistroempleados.registroempleados.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cidenetregistroempleados.registroempleados.Advice.UserNotFoundException;
import com.cidenetregistroempleados.registroempleados.dao.EmpleadoDAO;
import com.cidenetregistroempleados.registroempleados.entity.Empleado;
import com.cidenetregistroempleados.registroempleados.repository.RepositorioEmpleado;

@Service
public class ServicioEmpleadoImp implements ServicioEmpleado {

	private EmpleadoDAO empleadoDAO;

	@Autowired
	private RepositorioEmpleado repository;

	@Autowired
	public ServicioEmpleadoImp(EmpleadoDAO empleadoDAO) {

		this.empleadoDAO = empleadoDAO;

	}

	@Override
	@Transactional
	public List<Empleado> findAll() {

		return repository.findAll();
	}

	@Override
	@Transactional
	public Page<Empleado> findSorting(Integer offset, Integer pageSize, String field) {

		Page<Empleado> empleados = repository
				.findAll(PageRequest.of(offset, pageSize).withSort(Sort.by(Sort.Direction.ASC, field)));

		return empleados;
	}

	@Override
	@Transactional
	public Empleado findById(int id) throws Exception {

		Empleado empleado = empleadoDAO.findById(id);

		if (empleado == null) {

			throw new UserNotFoundException("Id de empleado no encontrado para Id " + id);

		} else
			return empleadoDAO.findById(id);
	}

	@Override
	@Transactional
	public void save(Empleado empleado) throws Exception {

		if (empleado.getId() != null) {

			throw new UserNotFoundException(
					"Para la operacion de creación de nuevo empleado no se requiere de un ID, pues este se genera"
							+ "automaticamente, si esta intentando actualizar in registro dirijase a la opción correspondiente");

		}

		empleado.setId(0);

		// Set id a 0 en caso de que se introduzca un id
		// empleado.setId(0);

		// metodo que crea el correo electronico 
		 
		empleado.setEmail(creadorEmail(empleado.getPrimerNombre(),empleado.getPrimerApellido() , empleado.getPaisEmpleo())); 
		
		

		// Estado activo

		empleado.setEstado("ACTIVO");

		empleadoDAO.save(empleado);

	}

	@Override
	@Transactional
	public void deleteById(int Id) throws Exception {

		Empleado tempEmpleado = empleadoDAO.findById(Id);

		if (tempEmpleado == null) {

			throw new UserNotFoundException("No se encontro el id seleccionado " + Id);

		} else

			empleadoDAO.deleteById(Id);

	}

	
	@Override
	@Transactional
	public void saveUpdate(Empleado empleado) throws Exception {

		if (empleado.getId() == null) {

			throw new UserNotFoundException("Id de empleado no ingresado");

		}

		Empleado empleadoTemp = empleadoDAO.findById(empleado.getId());

		if (empleadoTemp == null) {

			throw new UserNotFoundException("Registro de empleado no encontrado para Id: " + empleado.getId()
					+ " seleccionado, si desea agregar un nuevo registro" + " dirijase a la opcion correspondiente ");

		}

		// actualizacion de fecha registro

		
		
		empleado.setFechaRegistro(LocalDateTime.now());
		
		//actualiza correo electronico 
		
		empleado.setEmail(creadorEmail(empleado.getPrimerNombre(),empleado.getPrimerApellido() , empleado.getPaisEmpleo())); 

		empleadoDAO.saveUpdate(empleado);

	}
	
	
	@Transactional
	public String creadorEmail(String primerNombre, String primerApellido, String paisEmpleo) {
		
	
		String nombre = primerNombre.replaceAll("\\s", "").toLowerCase();
		String apellido = primerApellido.replaceAll("\\s", "").toLowerCase();
		
		
		if (paisEmpleo.equals("COL")) {
			StringBuilder builder = new StringBuilder();
			builder.append(nombre);
			builder.append(".");
			builder.append(apellido);
			builder.append("@cidenet.com.co");

			String tempEmail = builder.toString();

			if (!empleadoDAO.isEmailUnico(tempEmail)) {

				int newId = empleadoDAO.getLastId() + 1;

				StringBuilder builder2 = new StringBuilder();
				builder2.append(nombre);
				builder2.append(".");
				builder2.append(apellido);
				builder2.append(".");
				builder2.append(newId);
				builder2.append("@cidenet.com.co");

				return (builder2.toString());

			} else {
				return (tempEmail);
			}

		} else {
			StringBuilder builder = new StringBuilder();
			builder.append(nombre);
			builder.append(".");
			builder.append(apellido);
			builder.append("@cidenet.com.us");

			String tempEmail = builder.toString();

			if (!empleadoDAO.isEmailUnico(tempEmail)) {
				int newId = empleadoDAO.getLastId() + 1;

				StringBuilder builder2 = new StringBuilder();
				builder2.append(nombre);
				builder2.append(".");
				builder2.append(apellido);
				builder2.append(".");
				builder2.append(newId);
				builder2.append("@cidenet.com.us");
				
				return (builder2.toString());

			} else {
				return (tempEmail);
			}
		
		
		
	}
	

}

}
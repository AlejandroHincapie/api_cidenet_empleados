package com.cidenetregistroempleados.registroempleados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class RegistroempleadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistroempleadosApplication.class, args);
	}

}

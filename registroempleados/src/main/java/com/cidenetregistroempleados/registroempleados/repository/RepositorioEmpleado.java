package com.cidenetregistroempleados.registroempleados.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cidenetregistroempleados.registroempleados.entity.Empleado;

public interface RepositorioEmpleado extends JpaRepository<Empleado, Integer> {

}

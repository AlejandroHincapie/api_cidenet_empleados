package com.cidenetregistroempleados.registroempleados.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenetregistroempleados.registroempleados.entity.Empleado;
import com.cidenetregistroempleados.registroempleados.service.ServicioEmpleado;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class EmpleadoRestControlador {

	private ServicioEmpleado servicioEmpleado;

	@Autowired
	public EmpleadoRestControlador(ServicioEmpleado servicioEmpleado) {
		this.servicioEmpleado = servicioEmpleado;

	}

	// expose "/empleados" and return list of employees
	@GetMapping("/empleados")
	public List<Empleado> findAll() {
		return servicioEmpleado.findAll();
	}

	@GetMapping("empleados/{offset}/{pageSize}/{field}")
	public Page<Empleado> findSorting(@PathVariable Integer offset, @PathVariable Integer pageSize,
			@PathVariable String field) {

		return servicioEmpleado.findSorting(offset, pageSize, field);

	}

	// add mapping for get / empleados / empleadoId

	@GetMapping("/empleados/{empleadoId}")
	public Empleado getEmpleado(@PathVariable int empleadoId) throws Exception {

		Empleado empleado = servicioEmpleado.findById(empleadoId);

		return empleado;

	}

	// add mapping for POST /empleados - add new empleado

	@PostMapping("/empleados")
	public Empleado addEmpleado(@RequestBody @Valid Empleado empleado) throws Exception {

		servicioEmpleado.save(empleado);

		return empleado;

	}

	@PutMapping("/empleados")
	public Empleado updateEmpleado(@RequestBody @Valid Empleado empleado) throws Exception {

		servicioEmpleado.saveUpdate(empleado);

		return empleado;

	}

	@DeleteMapping("/empleados/{empleadoId}")
	public String deleteEmpleado(@PathVariable int empleadoId) throws Exception {

		servicioEmpleado.deleteById(empleadoId);

		return "Se elimino correctamente el empleado con ID " + empleadoId;

	}

}

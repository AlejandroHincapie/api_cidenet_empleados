package com.cidenetregistroempleados.registroempleados.entity;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.cidenetregistroempleados.registroempleados.validation.ValidateArea;
import com.cidenetregistroempleados.registroempleados.validation.ValidateCountry;
import com.cidenetregistroempleados.registroempleados.validation.ValidateEstado;
import com.cidenetregistroempleados.registroempleados.validation.ValidateIdentificacion;
import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="empleados", uniqueConstraints =  @UniqueConstraint(name="identificacionUnica" , columnNames= {"tipo_identificacion","numero_identificacion"}))
public class Empleado {
	
	//definir campos
	//implementar validacion
	
	@Id
	@Column(name="id", unique = true)
	private Integer id; 
	
	@NotBlank(message = "Nombre de empleado no puede estar vacio")
	@Pattern(regexp = "[A-Z ]{1,}", message = "Primer apellido, solo se admiten letras mayusculas sin caracteres especiales(acentos ni Ñ)")
	@Size(message = "Primer apellido longitud maxima de 20 caracteres", max = 20)
	@Column(name="primer_apellido")
	private String primerApellido;
	
	@NotBlank (message = "Apellido de empleado no puede estar vacio")
	@Pattern(regexp = "[A-Z ]{1,}", message = "Segundo apellido, solo se admiten letras mayusculas sin caracteres especiales(acentos ni Ñ)")
	@Size(message = "Segundo apellido longitud maxima de 20 caracteres", max = 20)
	@Column(name="segundo_apellido", length = 20)
	private String segundoApellido;

	@NotBlank (message = "Nombre de empleado no puede estar vacio")
	@Pattern(regexp = "[A-Z]{1,}", message = "Primer nombre, solo se admiten letras mayusculas sin caracteres especiales(acentos ni Ñ)")
	@Size(message = "Primer nombre longitud maxima de 20 caracteres", max = 20)
	@Column(name="primer_nombre", length = 20 )
	private String primerNombre;

	@Pattern(regexp = "[A-Z ]{1,}", message = "Error en otros nombres, solo se admiten letras mayusculas sin caracteres especiales(acentos ni Ñ)")
	@Size(message = "Otros nombres, longitud maxima de 50 caracteres", max = 50)
	@Column(name="otros_nombres")
	private String otrosNombres;

	@NotNull
	@ValidateCountry //CustomAnotation debe introducir un valor COL o USA
	@Column(name="pais_empleo")
	private String paisEmpleo;

	
	@NotNull
	@ValidateIdentificacion
	@Column(name="tipo_identificacion")
	private String tipoIdentificacion;

	@NotBlank
	@Size (message = "error en longitud de identificacion, maximo 20 caracteres", max=20)
	@Pattern (regexp = "[A-Za-z0-9-]{1,}", message = "Numero de identificacion: debe estar compuesto por numeros y/o letras (no se admiten caracteres especiales ni acentos) ")
	@Column(name="numero_identificacion")
	private String numeroIdentificacion;
	
	
	@Column(name="email", unique = true, length = 300)
	private String email;
		
	@PastOrPresent(message = "Fecha de ingreso a la empresa debe ser anterior o igual a la actual")	
	@JsonFormat(pattern = "dd-MM-yyyy")
	@Column(name="fecha_ingreso")
	private LocalDate fechaIngreso;
	
	
	@CreationTimestamp
	@Column(name="fecha_registro")
	private LocalDateTime fechaRegistro;
	
	
	
	@ValidateArea
	@Column(name="area")
	private String area;
	
	@NotNull
	@ValidateEstado
	@Column(name="estado")
	private String estado;


	
	
	//definir constructores

	public Empleado() {
		
	}
	
	

	public Empleado(Integer id, String primerApellido, String segundoApellido, String primerNombre, String otrosNombres,
			String paisEmpleo, String tipoIdentificacion, String numeroIdentificacion, LocalDate fechaIngreso,
			String area, String email,LocalDateTime fechaRegistro, String estado ) {
		this.id = id;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.primerNombre = primerNombre;
		this.otrosNombres = otrosNombres;
		this.paisEmpleo = paisEmpleo;
		this.tipoIdentificacion = tipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.email = email;
		this.fechaIngreso = fechaIngreso;
		this.fechaRegistro = fechaRegistro;
		this.area = area;
		this.estado = estado; 
	}


	//definir getters and setters

	public String getPrimerApellido() {
		return primerApellido;
	}




	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public String getSegundoApellido() {
		return segundoApellido;
	}




	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}




	public String getPrimerNombre() {
		return primerNombre;
	}




	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}




	public String getOtrosNombres() {
		return otrosNombres;
	}




	public void setOtrosNombres(String otrosNombres) {
		this.otrosNombres = otrosNombres;
	}




	public String getPaisEmpleo() {
		return paisEmpleo;
	}




	public void setPaisEmpleo(String paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}




	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}




	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}




	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}




	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}




	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}




	public void setFechaIngreso(LocalDate localDate) {
		this.fechaIngreso = localDate;
	}




	public String getArea() {
		return area;
	}




	public void setArea(String area) {
		this.area = area;
	}




	public Integer getId() {
		return id;
	}




	public String getEmail() {
		return email;
	}




	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}




	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado; 
		
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	
		
	

}

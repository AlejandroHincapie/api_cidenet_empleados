package com.cidenetregistroempleados.registroempleados.dao;


import com.cidenetregistroempleados.registroempleados.entity.Empleado;


import java.util.List;





public interface EmpleadoDAO{
	
	public List<Empleado> findAll(); 
	
	public Empleado findById (int Id);
	
	public void save(Empleado empleado); 
	
	public void saveUpdate(Empleado empleado); 
	
	public void deleteById(int Id); 
	
	public boolean isEmailUnico(String email); 
	
	public Integer getLastId (); 
	
	 
	
	

}

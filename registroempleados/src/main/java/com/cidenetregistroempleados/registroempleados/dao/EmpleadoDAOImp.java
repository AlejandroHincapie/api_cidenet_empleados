package com.cidenetregistroempleados.registroempleados.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cidenetregistroempleados.registroempleados.entity.Empleado;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;

@Repository
public class EmpleadoDAOImp implements EmpleadoDAO {

	// Definir campos para el manejo de las entidades

	private EntityManager entityManager;

	// configurar las inyecciones en los constructores

	@Autowired
	public EmpleadoDAOImp(EntityManager entityManager) {

		this.entityManager = entityManager;

	}

	@Override
	public List<Empleado> findAll() {

		// Crear un query

		Query query = entityManager.createQuery("from Empleado");

		// Ejecutar query y obtener resultados

		List<Empleado> empleados = query.getResultList();

		// regresar resultados
		
		
		

		return empleados;
	}

	@Override
	public Empleado findById(int id) {

		Empleado empleado = entityManager.find(Empleado.class, id);

		return empleado;
	}

	@Override
	public void save(Empleado empleado) {

		// save

		Empleado dbEmpleado = entityManager.merge(empleado);

		empleado.setId(dbEmpleado.getId());

	}

	@Override
	public void deleteById(int id) {

		Query query = entityManager.createQuery("delete from Empleado where id=:idEmpleado");

		query.setParameter("idEmpleado", id);

		query.executeUpdate();

	}

	@Override
	public boolean isEmailUnico(String email) {

		Query query = entityManager.createQuery("From Empleado WHERE email=:emailEmpleado").setParameter("emailEmpleado", email);

		List<Empleado> empleados = query.getResultList();
				
		if (empleados.size()==0) return true;
		else return false; 
	}
	
	
	

	@Override
	public Integer getLastId() {

		Query query = entityManager.createQuery("FROM Empleado WHERE id =(SELECT MAX(id) FROM Empleado)");

		List<Empleado> empleados = query.getResultList();

		if (empleados.size()==0) return 0;
		else return empleados.get(0).getId();

	}

	@Override
	public void saveUpdate(Empleado empleado) {
		
		Empleado dbEmpleado = entityManager.merge(empleado);

		empleado.setId(dbEmpleado.getId());
		
	}



}

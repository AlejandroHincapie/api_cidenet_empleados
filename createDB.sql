CREATE DATABASE  IF NOT EXISTS `registro_empleados`;
USE `registro_empleados`;


DROP TABLE IF EXISTS `empleados`;

CREATE TABLE `empleados` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
  `primer_apellido` varchar(20) NOT NULL,
  `segundo_apellido` varchar(20) NOT NULL,
   `primer_nombre` varchar(20) NOT NULL,
   `otros_nombres` varchar(50),
    `pais_empleo` varchar(20),
    `tipo_identificacion` varchar(20), 
    `numero_identificacion` varchar(20),
  `email` varchar(300) NOT NULL,
  `fecha_ingreso` DATE, 
  `fecha_registro` DATETIME, 
  `area` varchar(20) NOT NULL,
   `estado` varchar(20) NOT NULL,
      
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

